module thiss.de/goinpractice/chap2

go 1.16

require (
	github.com/braintree/manners v0.0.0-20160418043613-82a8879fc5fd
	github.com/go-ini/ini v1.62.0
	github.com/jessevdk/go-flags v1.5.0
	github.com/kylelemons/go-gypsy v1.0.0
	github.com/smartystreets/goconvey v1.6.4 // indirect
	gopkg.in/ini.v1 v1.62.0 // indirect
	gopkg.in/urfave/cli.v1 v1.20.0
)
